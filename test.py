from core.blockchain import Blockchain
from core.transaction import Transacction

kcoins = Blockchain()
for i in range(15):
    tx = Transacction("0x","nasser","zahra",i)
    kcoins.createNewTransaction(tx)
    currentBlock = kcoins.getCurrentData()
    prevHash = kcoins.getLastBlock().hash
    #print("prevHash", prevHash)
    nonce = kcoins.proofOfWork(prevHash,currentBlock)
    hash = kcoins.hashBlock(prevHash, currentBlock,nonce)
    kcoins.createNewBlock(nonce,prevHash,hash)