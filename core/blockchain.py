from core.block import Block
from core.transaction import Transacction
import hashlib

class Blockchain:
    def __init__(self):
        self.__chain = []
        self.__bendingTransactions = []
        self.createNewBlock(100,"0","0")
    def createNewBlock(self, nonce,prevHash,hash):
        block = Block(len(self.__chain)+1, prevHash,hash,self.__bendingTransactions, nonce)
        self.__bendingTransactions = []
        self.__chain.append(block)

    def createNewTransaction(self, tx):
        tx.id = "0x"
        self.__bendingTransactions.append(tx)
        return tx
    
    def getLastBlock(self):
        return self.__chain[len(self.__chain)-1]

    def getCurrentData(self):
        block = Block(len(self.__chain)+1, self.getLastBlock().hash,'',self.__bendingTransactions,0)
        return block

    def getChain(self):
        return self.__chain

    def hashBlock(self,prevHash,currentBlock,nonce):
        dataAsString = prevHash + str(nonce) + str(currentBlock)
        #print(dataAsString)
        hash = hashlib.sha256(str.encode(dataAsString))
        return hash.hexdigest()

    def proofOfWork(self,prevHash, currentBlock):
        nonce = 0
        hash = self.hashBlock(prevHash,currentBlock,nonce)
        while  hash[:5] != "00000":
            nonce+=1
            hash = self.hashBlock(prevHash,currentBlock,nonce)
        print(nonce)
        return nonce
            
    # return copy of open transaction
    def get_open_transactions(self):
        return self.__bendingTransactions[:]