from core.transaction import Transacction

"""A single block of our blockchain.

Attributes:
    :index: The index of this block.
    :previous_hash: The hash of the previous block in the blockchain.
    :transactions: A list of transaction which are included in the block.
    :nonce: The proof of work number that yielded this block.
"""
class Block:
    def __init__(self,index,prevHash,hash,transactions,nonce):
        self.index = index
        self.prevHash = prevHash
        self.hash = hash
        self.transactions = transactions
        self.nonce = nonce

    """ Convert the block to string( for hashin ), I use string instead of json, because  
        if I want to use another programming language like c++ or python, json output is not
        standard (exactlly same)
    """
    def __str__(self):
        text  = "index=" + str(self.index)
        text+= ", hash=" + self.hash
        text+= ", prevHash=" + self.prevHash
        text+= ", proof=" + str(self.nonce)
        text+= ", transactions="
        txs=""
        for tx in self.transactions:
            txs += '{' + str(tx) + '},'
        text+=txs
        return text
   