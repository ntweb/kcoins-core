class Transacction:
    def __init__(self, id,sender,recipient,amount):
        self.id = id
        self.sender = sender
        self.recipient = recipient
        self.amount = amount


    """Convert the transaction to string( for hashin ), I use string instead of json, because  
        if I want to use another programming language like c++ or python, json output is not
        standard (exactlly same)
    """
    def __str__(self):
        text  = "amount=" + str(self.amount)
        text+= ", sender=" + self.sender
        text+= ", recipient=" + self.recipient
        text+= ", id=" + self.id
        return text
   