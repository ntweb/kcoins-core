from core.blockchain import Blockchain
from core.transaction import Transacction
import hashlib
from flask import Flask,jsonify,request
from flask_cors import CORS

kcoins = Blockchain()
app = Flask(__name__)
CORS(app)


#transactions
@app.route('/api/tx/open_transactions', methods=['GET'])
def main():
    transactions = kcoins.get_open_transactions()
    txs_dict = [tx.__dict__ for tx in transactions]
    return jsonify(txs_dict), 200

@app.route('/api/tx/add', methods=['post'])
def add_transaction():
    required_fields = ['recipient', 'amount']
    values = request.get_json()
    if not values:
        response = {
            'message': 'No data found.'
        }
        return jsonify(response), 400
    elif not all(field in values for field in required_fields):
        response = {
            'message': 'Required data is missing.'
        }
        return jsonify(response), 400
    else:
        recipient = values['recipient']
        amount = values['amount']
        sender = values['sender']
        res = kcoins.createNewTransaction(Transacction("id", sender,recipient,amount))
        return jsonify(res.__dict__), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8081)

